# go-dynatrace

Go library to use the WebAPIs of Dynatrace.

### CAUTION:
Please only use the release, the main branch is undergoing heavy changes and therefore may break (or most likely won't currently work or crash).

I'm doing my best to make the library stable again and implement proper tests, this may take some time though.

Note that the API will break in the next release for sure, since I'm moving from my own implementaitons for the needs I had to "simply" reflecting the Dynatrace API. APIs will change, functions disappear etc.