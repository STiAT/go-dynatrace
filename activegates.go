package dynatrace

import (
	"encoding/json"
	"net/url"
)

// GetAllActiveGates https://www.dynatrace.com/support/help/dynatrace-api/environment-api/activegates/activegate-info/get-all
func (c *Config) GetAllActiveGates() (*ActiveGateList, error) {
	requrl := c.EnvironmentURL + "/api/v2/activeGates"

	result := ActiveGateList{}
	_, err := c.getRequest(requrl, &result, nil)
	if err != nil {
		return nil, err
	}

	return &result, nil
}

// GetActiveGate https://www.dynatrace.com/support/help/dynatrace-api/environment-api/activegates/activegate-info/get-activegate
func (c *Config) GetActiveGate(agId string) (*ActiveGate, error) {
	requrl := c.EnvironmentURL + "/api/v2/activeGates/" + url.QueryEscape(agId)

	result := ActiveGate{}
	_, err := c.getRequest(requrl, &result, nil)
	if err != nil {
		return nil, err
	}

	return &result, nil
}

// GetGlobalActiveGateAutoUpdateConfig https://www.dynatrace.com/support/help/dynatrace-api/environment-api/activegates/auto-update-config/get-global
func (c *Config) GetGlobalActiveGateAutoUpdateConfig() (*ActiveGateGlobalAutoUpdateConfig, error) {
	requrl := c.EnvironmentURL + "/api/v2/activeGates/autoUpdate"

	result := ActiveGateGlobalAutoUpdateConfig{}
	_, err := c.getRequest(requrl, &result, nil)
	if err != nil {
		return nil, err
	}

	return &result, nil
}

// PutGlobalActiveGateAutoUpdateConfig https://www.dynatrace.com/support/help/dynatrace-api/environment-api/activegates/auto-update-config/put-global
func (c *Config) PutGlobalActiveGateAutoUpdateConfig(config ActiveGateGlobalAutoUpdateConfig) error {
	requrl := c.EnvironmentURL + "/api/v2/activeGates/autoUpdate"

	jsonStr, _ := json.Marshal(config)
	err := c.putRequest(requrl, nil, jsonStr)
	if err != nil {
		return err
	}

	return nil
}

// GetActiveGateAutoUpdateConfig https://www.dynatrace.com/support/help/dynatrace-api/environment-api/activegates/auto-update-config/get-instance
func (c *Config) GetActiveGateAutoUpdateConfig(agId string) (*ActiveGateAutoUpdateConfig, error) {
	requrl := c.EnvironmentURL + "/api/v2/activeGates/autoUpdate?" + url.QueryEscape(agId)

	result := ActiveGateAutoUpdateConfig{}
	_, err := c.getRequest(requrl, &result, nil)
	if err != nil {
		return nil, err
	}

	return &result, nil
}

// PutActiveGateAutoUpdateConfig https://www.dynatrace.com/support/help/dynatrace-api/environment-api/activegates/auto-update-config/put-instance
func (c *Config) PutActiveGateAutoUpdateConfig(agId string, config ActiveGateAutoUpdateConfig) error {
	requrl := c.EnvironmentURL + "/api/v2/activeGates/autoUpdate?" + url.QueryEscape(agId)

	jsonStr, _ := json.Marshal(config)
	err := c.putRequest(requrl, nil, jsonStr)
	if err != nil {
		return err
	}

	return nil
}

// GetAllAutoUpdateJobs https://www.dynatrace.com/support/help/dynatrace-api/environment-api/activegates/auto-update-jobs/get-all-jobs
func (c *Config) GetAllAutoUpdateJobs(jobinfo UpdateJobsRequest) (*UpdateJobList, error) {
	requrl := c.EnvironmentURL + "/api/v2/activeGates/" + url.QueryEscape(jobinfo.AGId) + "/updateJobs"

	// we skip sanity check - dynatrace does that for us
	result := UpdateJobList{}
	_, err := c.getRequest(requrl, &result, jobinfo)
	if err != nil {
		return nil, err
	}

	return &result, nil
}

// GetAutoUpdateJob https://www.dynatrace.com/support/help/dynatrace-api/environment-api/activegates/auto-update-jobs/get-job
func (c *Config) GetAutoUpdateJob(agId string, jobId string) (*UpdateJob, error) {
	requrl := c.EnvironmentURL + "/api/v2/activeGates/" + url.QueryEscape(agId) + "/updateJobs/" + url.QueryEscape(jobId)

	// we skip sanity check - dynatrace does that for us
	result := UpdateJob{}
	_, err := c.getRequest(requrl, &result, nil)
	if err != nil {
		return nil, err
	}

	return &result, nil
}

// PostAutoUpdateJob https://www.dynatrace.com/support/help/dynatrace-api/environment-api/activegates/auto-update-jobs/post-job
func (c *Config) PostAutoUpdateJob(agId string, updatejob UpdateJob) (*UpdateJob, error) {
	requrl := c.EnvironmentURL + "/api/v2/activeGates/" + url.QueryEscape(agId) + "/updateJobs"

	// we skip sanity check - dynatrace does that for us
	result := UpdateJob{}
	err := c.postRequest(requrl, &result, nil)
	if err != nil {
		return nil, err
	}

	return &result, nil
}

// DeleteAutoUpdateJob https://www.dynatrace.com/support/help/dynatrace-api/environment-api/activegates/auto-update-jobs/delete-job
func (c *Config) DeleteAutoUpdateJob(agId string, jobid string) error {
	requrl := c.EnvironmentURL + "/api/v2/activeGates/" + url.QueryEscape(agId) + "/updateJobs"

	// we skip sanity check - dynatrace does that for us
	err := c.deleteRequest(requrl, nil, map[string]string{"jobId": jobid})
	if err != nil {
		return err
	}

	return nil
}

// GetActiveGatesWithAutoUpdateJobs https://www.dynatrace.com/support/help/dynatrace-api/environment-api/activegates/auto-update-jobs/get-activegates-jobs
func (c *Config) GetActiveGatesWithAutoUpdateJobs(ujr UpdateJobsRequest) (*UpdateJobsAll, error) {
	requrl := c.EnvironmentURL + "/api/v2/activeGates/updateJobs"

	// we skip sanity check - dynatrace does that for us
	res := UpdateJobsAll{}
	_, err := c.getRequest(requrl, &res, ujr)
	if err != nil {
		return nil, err
	}

	return &res, nil
}
