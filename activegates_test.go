package dynatrace

import (
	"testing"
)

func TestActiveGates(t *testing.T) {
	c, err := getTestConfig()
	if err != nil {
		t.Fatal("Cannot open test configuration file: test.json")
	}

	var ags *ActiveGateList
	t.Run("GetAllActiveGates", func(t1 *testing.T) {
		ags, err = c.GetAllActiveGates()
		if err != nil {
			t1.Fatalf("Could not fetch AG configuration: %v", err)
		}

		t1.Run("GetActiveGate", func(t2 *testing.T) {
			if ags == nil || len(*ags.ActiveGates) == 0 {
				t2.Skip("Skipping test: No ActiveGate found")
			}
			ag := (*ags.ActiveGates)[0]
			_, err := c.GetActiveGate(*(*ags.ActiveGates)[0].ID)
			if err != nil {
				t2.Fatalf("Could not get ActiveGate by ID: %s, %s (%v)", *ag.Hostname, *ag.ID, err)
			}
		})

		t1.Run("GetActiveGateAutoUpdateConfig", func(t2 *testing.T) {
			if ags == nil || len(*ags.ActiveGates) == 0 {
				t2.Skip("Skipping test: No ActiveGate found")
			}
			ag := (*ags.ActiveGates)[0]
			_, err = c.GetActiveGateAutoUpdateConfig(*ag.ID)
			if err != nil {
				t2.Fatalf("Could not get ActiveGate Update Config by ID: %s, %s (%v)", *ag.Hostname, *ag.ID, err)
			}
		})

		t1.Run("GetAllAutoUpdateJobs", func(t2 *testing.T) {
			if ags == nil || len(*ags.ActiveGates) == 0 {
				t2.Skip("Skipping test: No ActiveGate found")
			}

			var jobinfo UpdateJobsRequest
			ag := (*ags.ActiveGates)[0]
			jobinfo.AGId = *ag.ID

			jobs, err := c.GetAllAutoUpdateJobs(jobinfo)
			if err != nil {
				t2.Fatalf("Could not get ActiveGate UpdateJobs by ID: %s, %s (%v)", *ag.Hostname, *ag.ID, err)
			}

			t2.Run("GetAutoUpdateJob", func(t3 *testing.T) {
				if jobs == nil || len(*jobs.UpdateJobs) == 0 {
					t3.Skip("Skipping test: No AutoUpdate Jobs found")
				}

				job := (*jobs.UpdateJobs)[0]
				ag := (*ags.ActiveGates)[0]
				_, err = c.GetAutoUpdateJob(*ag.ID, *job.JobID)
				if err != nil {
					t3.Fatalf("Could not get AutoUpdatJob information for: %s, %s (%v)", *ag.Hostname, *job.JobID, err)
				}
			})
		})
	})
}

func TestGetGlobalActiveGateAutoUpdateConfig(t *testing.T) {
	c, err := getTestConfig()
	if err != nil {
		t.Fatal("Cannot open test configuration file: test.json")
	}

	_, err = c.GetGlobalActiveGateAutoUpdateConfig()
	if err != nil {
		t.Fatalf("Could not fetch global AutoUpdate configuration: %v", err)
	}
}

func TestGetActiveGatesWithAutoUpdateJobs(t *testing.T) {
	c, err := getTestConfig()
	if err != nil {
		t.Fatal("Cannot open test configuration file: test.json")
	}

	var ujr UpdateJobsRequest
	_, err = c.GetActiveGatesWithAutoUpdateJobs(ujr)
	if err != nil {
		t.Fatalf("Cannot get Active Gates with AutoUpdate jobs: %v ", err)
	}
}
