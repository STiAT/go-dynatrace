package dynatrace

func (c *Config) GetAlertingProfiles() (*StubList, error) {
	requrl := c.EnvironmentURL + "/api/config/v1/alertingProfiles"
	result := StubList{}
	_, err := c.getRequest(requrl, &result, nil)

	if err != nil {
		return nil, err
	}

	return &result, nil
}
