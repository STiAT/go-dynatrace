package dynatrace

import "testing"

func TestGetAlertingProfiles(t *testing.T) {
	c, err := getTestConfig()
	if err != nil {
		t.Fatal("Cannot open test configuration file: test.json")
	}

	_, err = c.GetAlertingProfiles()
	if err != nil {
		t.Fatalf("Could not fetch AlertingProfile configuration: %v", err)
	}
}
