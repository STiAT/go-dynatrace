package dynatrace

import "net/url"

// PutAnonymizationJob https://www.dynatrace.com/support/help/dynatrace-api/environment-api/anonymization/put-job
func (c *Config) PutAnonymizationJob(req AnonymizationJobRequest) (*AnonymizationIdResult, error) {
	requrl := c.EnvironmentURL + "/api/v1/anonymize/anonymizationJobs"
	res := AnonymizationIdResult{}
	err := c.putRequest(requrl, &res, req)
	if err != nil {
		return nil, err
	}
	return &res, nil
}

// GetAnonymizationJob https://www.dynatrace.com/support/help/dynatrace-api/environment-api/anonymization/get-job-status
func (c *Config) GetAnonymizationJob(requestId string) (*AnonymizationProgressResult, error) {
	requrl := c.EnvironmentURL + "/api/v1/anonymize/anonymizationJobs/" + url.QueryEscape(requestId)

	resp := AnonymizationProgressResult{}
	_, err := c.getRequest(requrl, &resp, nil)
	if err != nil {
		return nil, err
	}
	return &resp, nil
}
