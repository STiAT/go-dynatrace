package dynatrace

import "testing"

func TestGetAnonymizationJob(t *testing.T) {
	// TODO: Find a way to generate a non-saying job and test if it's completed
	// Reasoning: We do not have any JobID by default, there is no way to fetch all jobs.
	// We need to find something to create a job and fetch it again, but since that's tinkering with the environment it's currently unimplemented
}
