package dynatrace

import "net/url"

// GetAuditLogs https://www.dynatrace.com/support/help/dynatrace-api/environment-api/audit-logs/get-log
func (c *Config) GetAuditLogs(req AuditLogRequest) (*AuditLog, error) {
	requrl := c.EnvironmentURL + "/api/v2/auditlogs"

	resp := AuditLog{}
	_, err := c.getRequest(requrl, &resp, req) // we get nextpage in the struct according to docs
	if err != nil {
		return nil, err
	}

	return &resp, nil
}

// GetAuditLogEntry https://www.dynatrace.com/support/help/dynatrace-api/environment-api/audit-logs/get-entry
func (c *Config) GetAuditLogEntry(logid string) (*AuditLogEntry, error) {
	requrl := c.EnvironmentURL + "/api/v2/auditlogs/" + url.QueryEscape(logid)
	resp := AuditLogEntry{}
	_, err := c.getRequest(requrl, &resp, nil)
	if err != nil {
		return nil, err
	}
	return &resp, nil
}
