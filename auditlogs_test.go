package dynatrace

import "testing"

func TestAuditLogs(t *testing.T) {
	c, err := getTestConfig()
	if err != nil {
		t.Fatal("Cannot open test configuration file: test.json")
	}

	var al *AuditLog
	t.Run("GetAuditLogs", func(t1 *testing.T) {
		req := AuditLogRequest{}
		al, err = c.GetAuditLogs(req)
		if err != nil {
			t1.Fatalf("Could not fetch AG configuration: %v", err)
		}

		t1.Run("GetAuditLogEntry", func(t2 *testing.T) {
			if al == nil || len(*al.AuditLogs) == 0 {
				t2.Skip("Skipping test: No AuditLog entry found")
			}

			le := (*al.AuditLogs)[0]
			_, err := c.GetAuditLogEntry(*le.LogID)
			if err != nil {
				t2.Fatalf("Could not get Audit Log entry for LogID: %s", *le.LogID)
			}
		})
	})
}
