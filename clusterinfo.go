package dynatrace

// GetClusterTime https://www.dynatrace.com/support/help/dynatrace-api/environment-api/cluster-information
func (c *Config) GetClusterTime() (*string, error) {
	requrl := c.EnvironmentURL + "/api/v1/time"
	resp, err := c.getPlainRequest(requrl, nil)
	if err != nil {
		return nil, err
	}

	ret := string(*resp)
	return &ret, nil
}

// GetClusterVersion https://www.dynatrace.com/support/help/dynatrace-api/environment-api/cluster-information
func (c *Config) GetClusterVersion() (*ClusterVersion, error) {
	requrl := c.EnvironmentURL + "/api/v1/config/clusterversion"

	resp := ClusterVersion{}
	_, err := c.getRequest(requrl, &resp, nil)
	if err != nil {
		return nil, err
	}

	return &resp, nil
}
