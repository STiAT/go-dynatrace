package dynatrace

import "testing"

func TestGetClusterTime(t *testing.T) {
	c, err := getTestConfig()
	if err != nil {
		t.Fatal("Cannot open test configuration file: test.json")
	}

	t.Run("GetClusterTime", func(t *testing.T) {
		_, err = c.GetClusterTime()
		if err != nil {
			t.Fatalf("Could not fetch AG configuration: %v", err)
		}
	})
}

func TestGetClusterVersion(t *testing.T) {
	c, err := getTestConfig()
	if err != nil {
		t.Fatal("Cannot open test configuration file: test.json")
	}

	t.Run("GetClusterVersion", func(t *testing.T) {
		_, err = c.GetClusterVersion()
		if err != nil {
			t.Fatalf("Could not fetch Cluster Version: %v", err)
		}
	})
}
