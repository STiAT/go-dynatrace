package dynatrace

import (
	"net/url"
)

// CreateEntity "Create custom device" API from dynatrace
// Documentation: https://www.dynatrace.com/support/help/dynatrace-api/environment-api/topology-and-smartscape/custom-device-api/create-custom-device-via-dynatrace-api/
func (c *Config) CreateEntity(hostname string, assetid string, group string, techtype string) (*string, error) {
	requrl := c.EnvironmentURL + "/api/v1/entity/infrastructure/custom/" + url.QueryEscape(assetid)

	var postvar struct {
		DisplayName string `json:"displayName"`
		Group       string `json:"group"`
		Type        string `json:"type"`
	}

	postvar.DisplayName = hostname
	postvar.Group = group
	postvar.Type = techtype

	res := CustomDevicePushResult{}
	err := c.postRequest(requrl, &res, postvar)
	if err != nil {
		return nil, err
	}

	return res.EntityID, nil
}
