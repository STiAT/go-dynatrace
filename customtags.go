package dynatrace

// GetCustomTags https://www.dynatrace.com/support/help/dynatrace-api/environment-api/custom-tags/get-tags
func (c *Config) GetCustomTags(getreq CustomTagRequest) (*CustomEntityTags, error) {
	requrl := c.EnvironmentURL + "/api/v2/tags"

	resp := CustomEntityTags{}
	_, err := c.getRequest(requrl, resp, getreq)
	if err != nil {
		return nil, err
	}

	return &resp, nil
}

// PostCustomTags https://www.dynatrace.com/support/help/dynatrace-api/environment-api/custom-tags/post-tags
func (c *Config) PostCustomTags(postreq CustomTagRequest) (*AddedEntityTags, error) {
	requrl := c.EnvironmentURL + "/api/v2/tags"

	resp := AddedEntityTags{}
	err := c.postRequest(requrl, &resp, postreq)
	if err != nil {
		return nil, err
	}

	return &resp, nil
}

// DeleteCustomTags https://www.dynatrace.com/support/help/dynatrace-api/environment-api/custom-tags/del-tags
func (c *Config) DeleteCustomTags(delreq CustomTagDeleteRequest) (*DeletedEntityTags, error) {
	requrl := c.EnvironmentURL + "/api/v2/tags"

	resp := DeletedEntityTags{}
	err := c.postRequest(requrl, &resp, delreq)
	if err != nil {
		return nil, err
	}

	return &resp, nil
}
