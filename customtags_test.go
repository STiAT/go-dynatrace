package dynatrace

import (
	"testing"
)

func TestGetCustomTags(t *testing.T) {
	c, err := getTestConfig()

	if err != nil {
		t.Fatal("Cannot open test configuration file: test.json")
	}

	var getreq CustomTagRequest
	// go for fetching all hosts, should return empty if there are no hosts in the environment
	getreq.EntitySelector = `type("HOST")`
	_, err = c.GetCustomTags(getreq)
	if err != nil {
		t.Fatalf("Could not fetch AG configuration: %v", err)
	}
}
