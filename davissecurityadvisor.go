package dynatrace

// DavisSecurityAdvisor https://www.dynatrace.com/support/help/dynatrace-api/environment-api/davis-security-advice
func (c *Config) DavisSecurityAdvisor(req DavisSecurityAdvisorRequest) (*DavisSecurityAdviceList, error) {
	requrl := c.EnvironmentURL + "/api/v2/davis/securityAdvices"

	resp := DavisSecurityAdviceList{}
	_, err := c.getRequest(requrl, &resp, req) // ignore nextpagekey, we get it in the structure if docs are correct
	if err != nil {
		return nil, err
	}

	return &resp, nil
}
