package dynatrace

import (
	"bytes"
	"crypto/tls"
	"encoding/json"
	"errors"
	"io"
	"net/http"
	"net/url"
	"strconv"
	"time"
)

// Config is the base object for the Dynatrace information required to query the Server
type Config struct {
	EnvironmentURL        string  `json:"environmenturl"`
	Token                 string  `json:"token"`
	PageSize              float64 `json:"pagesize"`
	Timeout               float64 `json:"timeout"`
	TLSInsecureSkipVerify bool    `json:"tlsinsecureskipverify"`
}

// Error object by dynatrace
type Error struct {
	Error struct {
		Code    float64 `json:"code"`
		Message string  `json:"message"`
	} `json:"error"`
}

func (c *Config) getRequest(requrl string, target interface{}, params interface{}) (*string, error) {
	req, _ := http.NewRequest("GET", requrl, nil)

	q := getQuery(req, params)

	req.URL.RawQuery = q.Encode()

	req.Close = true
	req.Header.Add("Authorization", "Api-Token "+c.Token)

	client := &http.Client{
		Timeout: time.Duration(c.Timeout) * time.Second,
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{
				InsecureSkipVerify: c.TLSInsecureSkipVerify,
			},
		},
	}

	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()

	nextpage, err := getResponseValue(resp, target)
	if err != nil {
		return nil, err
	}

	return nextpage, nil
}

func (c *Config) getPlainRequest(requrl string, params interface{}) (*[]byte, error) {
	req, _ := http.NewRequest("GET", requrl, nil)

	q := getQuery(req, params)

	req.URL.RawQuery = q.Encode()

	req.Close = true
	req.Header.Add("Authorization", "Api-Token "+c.Token)

	client := &http.Client{
		Timeout: time.Duration(c.Timeout) * time.Second,
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{
				InsecureSkipVerify: c.TLSInsecureSkipVerify,
			},
		},
	}

	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	return &body, nil
}

func (c *Config) postRequest(url string, target interface{}, payload interface{}) error {
	jsonstr, err := json.Marshal(payload)
	if err != nil {
		return err
	}
	req, _ := http.NewRequest("POST", url, bytes.NewBuffer(jsonstr))
	req.Close = true
	req.Header.Add("Authorization", "Api-Token "+c.Token)
	req.Header.Add("Content-Type", "application/json")

	client := &http.Client{
		Timeout: time.Duration(c.Timeout) * time.Second,
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{
				InsecureSkipVerify: c.TLSInsecureSkipVerify,
			},
		},
	}

	resp, err := client.Do(req)
	if err != nil {
		return err
	}

	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	_, err = getResponseValue(resp, nil)
	if err != nil {
		return err
	}

	err = json.Unmarshal(body, &target)
	if err != nil {
		return err
	}
	return nil
}

func (c *Config) putRequest(url string, target interface{}, payload interface{}) error {
	jsonstr, err := json.Marshal(payload)
	if err != nil {
		return err
	}
	req, _ := http.NewRequest("PUT", url, bytes.NewBuffer(jsonstr))
	req.Close = true
	req.Header.Add("Authorization", "Api-Token "+c.Token)
	req.Header.Add("Content-Type", "application/json")

	client := &http.Client{
		Timeout: time.Duration(c.Timeout) * time.Second,
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{
				InsecureSkipVerify: c.TLSInsecureSkipVerify,
			},
		},
	}

	resp, err := client.Do(req)
	if err != nil {
		return err
	}

	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	_, err = getResponseValue(resp, nil)
	if err != nil {
		return err
	}

	err = json.Unmarshal(body, &target)
	if err != nil {
		return err
	}

	return nil
}

func (c *Config) deleteRequest(requrl string, target interface{}, params interface{}) error {
	req, _ := http.NewRequest("DELETE", requrl, nil)
	req.Close = true
	req.Header.Add("Authorization", "Api-Token "+c.Token)

	client := &http.Client{
		Timeout: time.Duration(c.Timeout) * time.Second,
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{
				InsecureSkipVerify: c.TLSInsecureSkipVerify,
			},
		},
	}

	q := getQuery(req, params)
	req.URL.RawQuery = q.Encode()

	resp, err := client.Do(req)
	if err != nil {
		return err
	}

	defer resp.Body.Close()

	_, err = getResponseValue(resp, target)
	if err != nil {
		return err
	}

	return nil
}

func getResponseValue(resp *http.Response, target interface{}) (*string, error) {
	var probe Error
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	nextpage := resp.Header.Get("Next-Page-Key")

	check := make(map[string]interface{})
	err = json.Unmarshal(body, &check)
	if err == nil {
		if v, ok := check["nextPageKey"]; ok {
			nextpage = v.(string)
		}
	}

	err = json.Unmarshal(body, &probe)

	if err == nil && probe.Error.Code != 0 && probe.Error.Message != "" {
		// check for error code type
		switch resp.StatusCode {
		case 400:
			return &nextpage, errors.New("Bad request: " + probe.Error.Message)
		case 401:
			return &nextpage, errors.New("Unauthorized: " + probe.Error.Message)
		case 404:
			return &nextpage, errors.New("Not found: " + probe.Error.Message)
		case 429:
			return &nextpage, errors.New("Too many requests: " + probe.Error.Message)
		default:
			return &nextpage, errors.New("Unknown error code (" + resp.Status + "): " + probe.Error.Message)
		}
	}

	if target != nil {
		err = json.Unmarshal(body, &target)
		if err != nil {
			return nil, err
		}
	}

	return &nextpage, nil
}

func (c *Config) postRawRequest(url string, data []byte) error {
	req, _ := http.NewRequest("POST", url, bytes.NewBuffer(data))
	req.Close = true
	req.Header.Add("Authorization", "Api-Token "+c.Token)
	req.Header.Add("Content-Type", "text/plain")

	client := &http.Client{
		Timeout: time.Duration(c.Timeout) * time.Second,
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{
				InsecureSkipVerify: c.TLSInsecureSkipVerify,
			},
		},
	}

	resp, err := client.Do(req)
	if err != nil {
		return err
	}

	defer resp.Body.Close()

	body, _ := io.ReadAll(resp.Body)
	if resp.StatusCode != 202 {
		return errors.New("Error " + strconv.Itoa(resp.StatusCode) + ": " + string(body))
	}

	return nil
}

func getQuery(req *http.Request, params interface{}) url.Values {
	q := req.URL.Query()
	var pinter map[string]interface{}
	inrec, _ := json.Marshal(params)
	json.Unmarshal(inrec, &pinter)
	for param, val := range pinter {
		switch val := val.(type) {
		case string:
			q.Add(param, val)
		case bool:
			q.Add(param, strconv.FormatBool(val))
		}
	}

	return q
}
