package dynatrace

import (
	"fmt"
	"strings"
)

// GetAllEntities gets all the entites by entity type (etype)
func (c *Config) GetAllEntities(fields []string) (*EntitiesList, error) {
	requrl := c.EnvironmentURL + "/api/v2/entities"

	types, err := c.GetAllEntityTypes()
	if err != nil {
		return nil, err
	}

	result := EntitiesList{}
	for _, t := range *types.Types {
		params := map[string]interface{}{
			"entitySelector": "type(\"" + *t.Type + "\")",
			"pageSize":       fmt.Sprintf("%d", int(c.PageSize)),
			"fields":         strings.Join(fields, ","),
		}

		tresult := EntitiesList{}
		nextpage, err := c.getRequest(requrl, &tresult, params)
		if err != nil {
			return nil, err
		}

		*result.Entities = append(*result.Entities, *tresult.Entities...)

		for nextpage != nil {
			tmp := EntitiesList{}
			nextpage, err = c.getRequest(requrl, &tmp, map[string]interface{}{"nextPageKey": *nextpage})
			if err != nil {
				return nil, err
			}

			*result.Entities = append(*result.Entities, *tmp.Entities...)
		}
	}

	return &result, nil
}

// GetAllEntitiesByName gets all the entites by entity type (etype)
func (c *Config) GetAllEntitiesByName(name string, fields []string) (*EntitiesList, error) {
	requrl := c.EnvironmentURL + "/api/v2/entities"

	types, err := c.GetAllEntityTypes()
	if err != nil {
		return nil, err
	}

	result := EntitiesList{}
	for _, t := range *types.Types {
		params := map[string]interface{}{
			"entitySelector": "type(\"" + *t.Type + "\"),entityName(\"" + name + "\")",
			"pageSize":       fmt.Sprintf("%d", int(c.PageSize)),
			"fields":         strings.Join(fields, ","),
		}
		tresult := EntitiesList{}

		nextpage, err := c.getRequest(requrl, &tresult, params)
		if err != nil {
			return nil, err
		}

		*result.Entities = append(*result.Entities, *tresult.Entities...)

		for nextpage != nil {
			tmp := EntitiesList{}
			nextpage, err = c.getRequest(requrl, &tmp, map[string]interface{}{"nextPageKey": *nextpage})

			if err != nil {
				return nil, err
			}

			*result.Entities = append(*result.Entities, *tmp.Entities...)
		}
	}

	return &result, nil
}

// GetAllEntitiesByTags returns all entities with the given tags
func (c *Config) GetAllEntitiesByTags(tags []string, fields []string) (*EntitiesList, error) {
	tagurl := ""
	for _, tag := range tags {
		tagurl = tagurl + ",tag(\"" + tag + "\")"
	}

	requrl := c.EnvironmentURL + "/api/v2/entities"

	types, err := c.GetAllEntityTypes()
	if err != nil {
		return nil, err
	}

	result := EntitiesList{}
	for _, t := range *types.Types {
		params := map[string]interface{}{
			"entitySelector": "type(\"" + *t.Type + "\")" + tagurl,
			"pageSize":       fmt.Sprintf("%d", int(c.PageSize)),
			"fields":         strings.Join(fields, ","),
		}

		result := EntitiesList{}
		nextpage, err := c.getRequest(requrl, &result, params)

		if err != nil {
			return nil, err
		}

		for nextpage != nil {
			tmp := EntitiesList{}
			nextpage, err = c.getRequest(requrl, &tmp, map[string]interface{}{"nextPageKey": *nextpage})

			if err != nil {
				return nil, err
			}

			*result.Entities = append(*result.Entities, *tmp.Entities...)
		}
	}

	return &result, nil
}

// GetEntityById returns all entities with the given tags
func (c *Config) GetEntityById(eid string) (*Entity, error) {
	requrl := c.EnvironmentURL + "/api/v2/entities/" + eid

	result := Entity{}
	_, err := c.getRequest(requrl, &result, nil)

	if err != nil {
		return nil, err
	}

	return &result, nil
}

func (c *Config) GetAllPGIByName(name string, fields []string) (*EntitiesList, error) {
	requrl := c.EnvironmentURL + "/api/v2/entities"

	params := map[string]interface{}{
		"entitySelector": "type(\"PROCESS_GROUP_INSTANCE\"),entityName(\"" + name + "\")",
		"pageSize":       fmt.Sprintf("%d", int(c.PageSize)),
		"fields":         strings.Join(fields, ","),
	}
	result := EntitiesList{}

	nextpage, err := c.getRequest(requrl, &result, params)

	if err != nil {
		return nil, err
	}

	for nextpage != nil {
		tmp := EntitiesList{}
		nextpage, err = c.getRequest(requrl, &tmp, map[string]interface{}{"nextPageKey": *nextpage})

		if err != nil {
			return nil, err
		}

		*result.Entities = append(*result.Entities, *tmp.Entities...)
	}

	return &result, nil
}

func (c *Config) GetAllEntityTypes() (*EntityTypeList, error) {
	requrl := c.EnvironmentURL + "/api/v2/entityTypes"

	result := EntityTypeList{}

	nextpage, err := c.getRequest(requrl, &result, nil)

	if err != nil {
		return nil, err
	}

	for nextpage != nil {
		tmp := EntityTypeList{}
		nextpage, err = c.getRequest(requrl, &tmp, map[string]interface{}{"nextPageKey": *nextpage})

		if err != nil {
			return nil, err
		}

		*result.Types = append(*result.Types, *tmp.Types...)
	}

	return &result, nil
}
