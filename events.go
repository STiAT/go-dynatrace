package dynatrace

import (
	"time"
)

/*
{
  "eventType":"AVAILABILITY_EVENT",
  "timeoutMinutes":120,
  "attachRules":{
    "entityIds": [
      "CUSTOM_DEVICE-C5B6EDF1498B1D3D"
    ]
  },
  "source":"SLA Check",
  "title":"SLA-Check: Ping failed.",
  "description":"Couldn't resolve hostname."
}
*/

// PostEvent is the "POST an event" API from dynatrace
// Documentation: https://www.dynatrace.com/support/help/dynatrace-api/environment-api/events/post-event
func (c *Config) PostEvent(entityid string, eventtype string, source string, title string, description string, timeout int, refresh bool) error {
	requrl := c.EnvironmentURL + "/api/v1/events"

	var postvar EventCreation
	*postvar.EventType = eventtype
	if !refresh {
		*postvar.Start = float64(time.Now().UnixNano() / 1000 / 1000)
	}
	*postvar.TimeoutMinutes = float64(timeout)
	*postvar.AttachRules.EntityIDs = []string{entityid}
	*postvar.Source = source
	*postvar.Title = title
	*postvar.Description = description

	result := EventStoreResult{}
	err := c.postRequest(requrl, &result, postvar)
	return err
}
