package dynatrace

import (
	"net/url"
	"strings"
)

// GetHost is the "Get a host" API from Dynatrace
// Documentation: https://www.dynatrace.com/support/help/dynatrace-api/environment-api/topology-and-smartscape/hosts-api/get-a-host/
func (c *Config) GetHost(id string) (*Host, error) {
	requrl := c.EnvironmentURL + "/api/v1/entity/infrastructure/hosts/" + url.QueryEscape(id)

	result := Host{}
	_, err := c.getRequest(requrl, &result, nil)

	if err != nil {
		return nil, err
	}

	return &result, nil
}

// GetAllHosts is the "get all" fromt he Dynatrace API.
// Documentation: https://www.dynatrace.com/support/help/dynatrace-api/environment-api/topology-and-smartscape/hosts-api/get-all/
func (c *Config) GetAllHosts() (*[]Host, error) {
	requrl := c.EnvironmentURL + "/api/v1/entity/infrastructure/hosts"

	result := []Host{}
	nextpage, err := c.getRequest(requrl, &result, map[string]interface{}{"showMonitoringCandidates": "true"})

	if err != nil {
		return nil, err
	}

	for nextpage != nil {
		tmp := []Host{}
		nextpage, err = c.getRequest(requrl, &tmp, map[string]interface{}{"nextPageKey": *nextpage})

		if err != nil {
			return nil, err
		}

		result = append(result, tmp...)
	}

	return &result, nil
}

// GetAllHostZones returns a simple map which states host => zone
func (c *Config) GetAllHostZones() (map[string]string, error) {
	hosts, err := c.GetAllHosts()
	if err != nil {
		return nil, err
	}

	var hostzones = make(map[string]string)

	for _, hostinfo := range *hosts {
		var hostname string
		if idx := strings.IndexByte(*hostinfo.DiscoveredName, '.'); idx >= 0 {
			hostname = (*hostinfo.DiscoveredName)[:strings.IndexByte(*hostinfo.DiscoveredName, '.')]
		} else {
			hostname = *hostinfo.DiscoveredName
		}

		// Custom Devices
		if hostname == "" {
			hostname = *hostinfo.DisplayName
		}

		hostzones[strings.ToLower(hostname)] = *hostinfo.NetworkZoneID
	}

	return hostzones, nil
}
