package dynatrace

import (
	"net/url"
	"strings"
	"time"
)

// GetEntitiesInMaintenance Get a list of hosts which are currently in Maintenance
func (c *Config) GetEntitiesInMaintenance() (*[]string, error) {
	maintenances, err := c.GetAllMaintenances()

	if err != nil {
		return nil, err
	}

	entities, err := c.GetAllEntities([]string{"tags"})
	if err != nil {
		return nil, err
	}

	var res []string

	// first, just check for global MMs, faster
	for _, maint := range *maintenances {
		// if the maintenance is currently not active - ignore it
		if !c.IsMaintActive(maint) {
			continue
		}

		// we won't conform with dynatrace standards here, we just scramble all into a big hostname list
		// check if it's a GlobalMM
		if c.IsGlobalMM(maint) {
			res := []string{}
			for _, entity := range *entities.Entities {
				res = append(res, *entity.EntityID)
			}
			return &res, nil
		}
	}

	// now check individual MMs, a bit slower, but we try to optimize this
	for _, maint := range *maintenances {
		// if the maintenance is currently not active - ignore it
		if !c.IsMaintActive(maint) {
			continue
		}

		// we want entities, so we go through all entities
		for _, entity := range *entities.Entities {
			if isEntityInMaintenance(entity, maint) {
				res = append(res, *entity.EntityID)
			}
		}
	}

	unique(res)
	return &res, nil
}

func unique(intSlice []string) *[]string {
	keys := make(map[string]bool)
	list := []string{}
	for _, entry := range intSlice {
		if _, value := keys[entry]; !value {
			keys[entry] = true
			list = append(list, entry)
		}
	}
	return &list
}

func isEntityInMaintenance(entity Entity, maint MaintenanceWindow) bool {
	for _, m := range *maint.Scope.Entities {
		if m == *entity.EntityID {
			return true
		}
	}

	if len(*maint.Scope.Matches) == 0 {
		return false
	}

	for _, f := range *maint.Scope.Matches {
		if *f.TagCombination == "OR" {
			if doesMatchOrFilter(entity, f) {
				return true
			}
		} else if *f.TagCombination == "AND" {
			if doesMatchAndFilter(entity, f) {
				return true
			}
		}
	}

	return false
}

func doesMatchAndFilter(entity Entity, mtags MonitoredEntityFilter) bool {
	for _, mt := range *mtags.Tags {
		found := false
		for _, et := range *entity.Tags {
			if mt.Key == et.Key && mt.Value == et.Value {
				found = true
				break
			}
		}
		if !found {
			return false
		}
	}

	return true
}

func doesMatchOrFilter(entity Entity, mtags MonitoredEntityFilter) bool {
	for _, mt := range *mtags.Tags {
		for _, et := range *entity.Tags {
			if mt.Key == et.Key && mt.Value == et.Value {
				return true
			}
		}
	}
	return false
}

// GetAllMaintenances is the "GET all maintenancne windows" API from dynatrace
// Documentation: https://www.dynatrace.com/support/help/dynatrace-api/configuration-api/maintenance-windows-api/get-all/
func (c *Config) GetAllMaintenances() (*[]MaintenanceWindow, error) {
	requrl := c.EnvironmentURL + "/api/config/v1/maintenanceWindows"

	result := StubList{}
	_, err := c.getRequest(requrl, &result, nil)

	if err != nil {
		return nil, err
	}

	// We're actually not interested in the stubs, but in the real Maintenances.
	var maints []MaintenanceWindow

	for _, maint := range *result.Values {
		maintentities, err := c.GetMaintenance(*maint.ID)
		if err != nil {
			return nil, err
		}
		maints = append(maints, *maintentities)
	}

	return &maints, nil
}

// GetMaintenance is the "GET a maintenance window" API from dynatrace
// Documentation: https://www.dynatrace.com/support/help/dynatrace-api/configuration-api/maintenance-windows-api/get-mw/
func (c *Config) GetMaintenance(id string) (*MaintenanceWindow, error) {
	requrl := c.EnvironmentURL + "/api/config/v1/maintenanceWindows/" + url.QueryEscape(id)

	maint := MaintenanceWindow{}
	_, err := c.getRequest(requrl, &maint, nil)

	if err != nil {
		return nil, err
	}

	return &maint, nil
}

// IsMaintActive checks weather a maintenance is active or not - handy function so exported
func (c *Config) IsMaintActive(maint MaintenanceWindow) bool {
	switch rectype := *maint.Schedule.RecurrenceType; rectype {
	case "ONCE":
		// use the timezone in dynatrace for the start/end date
		// we do not require this on others since we use the Now().Format() to get dynatrace believe all is UTC
		loc, err := time.LoadLocation(*maint.Schedule.ZoneID)
		if err != nil {
			loc, _ = time.LoadLocation("Local")
		}

		start, _ := time.ParseInLocation("2006-01-02 15:04", *maint.Schedule.Start, loc)
		end, _ := time.ParseInLocation("2006-01-02 15:04", *maint.Schedule.End, loc)
		if time.Now().After(start) && time.Now().Before(end) {
			return true
		}
		return false
		//test
	case "DAILY":
		start, _ := time.Parse("15:04", *maint.Schedule.Recurrence.StartTime)
		end := start.Add(time.Minute * time.Duration(*maint.Schedule.Recurrence.DurationMinutes))
		currentTime, _ := time.Parse("15:04", time.Now().Format("15:04"))
		if currentTime.After(start) && currentTime.Before(end) {
			return true
		}
		return false
		/*
			recurrence": {
				"dayOfWeek": null,
				"dayOfMonth": null,
				"startTime": "13:26",
				"durationMinutes": 60
			},
		*/
	case "WEEKLY":
		if strings.EqualFold(time.Now().Weekday().String(), *maint.Schedule.Recurrence.DayOfWeek) {
			start, _ := time.Parse("15:04", *maint.Schedule.Recurrence.StartTime)
			end := start.Add(time.Minute * time.Duration(*maint.Schedule.Recurrence.DurationMinutes))
			currentTime, _ := time.Parse("15:04", time.Now().Format("15:04"))
			if currentTime.After(start) && currentTime.Before(end) {
				return true
			}
		}
		return false
		/*
			"recurrence": {
				"dayOfWeek": "MONDAY",
				"dayOfMonth": null,
				"startTime": "13:26",
				"durationMinutes": 60
			},
		*/
	case "MONTHLY":
		t := time.Now()
		if t.Day() == int(*maint.Schedule.Recurrence.DayOfMonth) {
			start, _ := time.Parse("15:04", *maint.Schedule.Recurrence.StartTime)
			end := start.Add(time.Minute * time.Duration(*maint.Schedule.Recurrence.DurationMinutes))
			currentTime, _ := time.Parse("15:04", time.Now().Format("15:04"))
			if currentTime.After(start) && currentTime.Before(end) {
				return true
			}
		}
		return false
		/*
			"recurrence": {
				"dayOfWeek": null,
				"dayOfMonth": 1,
				"startTime": "13:26",
				"durationMinutes": 60
			},
		*/
	}

	return false
}

// PostMaintenanceWindow adds a new maintenance window according to the configuration
func (c *Config) PostMaintenanceWindow(mm MaintenanceWindow) error {
	requrl := c.EnvironmentURL + "/api/config/v1/maintenanceWindows/"

	result := EntityShortRepresentation{}
	err := c.postRequest(requrl, &result, mm)

	return err
}

// IsGlobalMM is a helper function which checks if a MM is global (or not)
func (c *Config) IsGlobalMM(maint MaintenanceWindow) bool {
	// check for GlobalMM
	if len(*maint.Scope.Entities) == 0 && len(*maint.Scope.Matches) == 0 {
		// send mail - do shit.
		return true
	}

	// let's find out badly set MMs
	for _, v := range *maint.Scope.Matches {
		if *v.Type == "" && *v.MZID == "" && len(*v.Tags) == 0 {
			// badly set GlobalMM
			return true
		}
	}

	return false
}
