package dynatrace

func (c *Config) IngestMetric(metric string, dimensions, value string) error {
	requrl := c.EnvironmentURL + "/api/v2/metrics/ingest"
	rawdata := metric + "," + dimensions + " " + value

	err := c.postRawRequest(requrl, []byte(rawdata))

	if err != nil {
		return err
	}
	return nil
}
