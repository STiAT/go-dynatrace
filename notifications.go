package dynatrace

func (c *Config) GetAllNotifications() (*[]NotificationConfig, error) {
	requrl := c.EnvironmentURL + "/api/config/v1/notifications"
	result := NotificationConfigStubListDto{}
	_, err := c.getRequest(requrl, &result, nil)

	if err != nil {
		return nil, err
	}

	retval := []NotificationConfig{}
	for _, notification := range *result.Values {
		n, err := c.GetNotificationDetails(*notification.ID)
		if err != nil {
			return nil, err
		}
		retval = append(retval, *n)
	}

	return &retval, nil
}

func (c *Config) GetNotificationDetails(id string) (*NotificationConfig, error) {
	requrl := c.EnvironmentURL + "/api/config/v1/notifications/" + id
	result := NotificationConfig{}
	_, err := c.getRequest(requrl, &result, nil)

	if err != nil {
		return nil, err
	}

	return &result, nil
}
