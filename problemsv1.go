package dynatrace

// GetActiveProblemsForEntity returns the relevant problems from a certain source
func (c *Config) GetActiveProblemsForEntity(entityid string, source string, eventtype string) (*[]string, error) {
	pf, err := c.GetProblemFeed()

	if err != nil {
		return nil, err
	}

	var retval []string

	for _, problem := range *pf.Result.Problems {
		// we don't bother with closed problems
		if *problem.Status == "CLOSED" {
			continue
		}

		p, err := c.GetProblemDetails(*problem.ID)
		if err != nil {
			return nil, err
		}

		for _, ev := range *p.Result.RankedEvents {
			if *ev.EntityID == entityid && *ev.Source == source && *ev.EventType == eventtype {
				// append if it does not exist
				retval = appendIfMissing(retval, *problem.ID)
			}
		}
	}

	return &retval, nil
}

// GetProblemFeed is the "GET Problem Feed" API call from dynatrace
func (c *Config) GetProblemFeed() (*ProblemFeedResultWrapper, error) {
	requrl := c.EnvironmentURL + "/api/v1/problem/feed"

	result := ProblemFeedResultWrapper{}
	_, err := c.getRequest(requrl, &result, nil)

	if err != nil {
		return nil, err
	}

	return &result, nil
}

// GetProblemDetails is the "GET details" API call from dynatrace
func (c *Config) GetProblemDetails(problemid string) (*ProblemDetailsResultWrapper, error) {
	requrl := c.EnvironmentURL + "/api/v1/problem/details/" + problemid

	result := ProblemDetailsResultWrapper{}
	_, err := c.getRequest(requrl, &result, nil)

	if err != nil {
		return nil, err
	}

	return &result, nil
}
