package dynatrace

import (
	"fmt"
	"time"
)

func (c *Config) GetOpenProblems() (*ProblemsDto, error) {
	requrl := c.EnvironmentURL + "/api/v2/problems"

	params := map[string]interface{}{
		"problemSelector": "status(\"open\")",
	}

	result := ProblemsDto{}
	nextpage, err := c.getRequest(requrl, &result, params)
	if err != nil {
		return nil, err
	}

	for nextpage != nil {
		tmp := ProblemsDto{}
		nextpage, err = c.getRequest(requrl, &tmp, map[string]interface{}{"nextPageKey": *nextpage})

		if err != nil {
			return nil, err
		}

		*result.Problems = append(*result.Problems, *tmp.Problems...)
	}

	return &result, nil
}

func (c *Config) GetAllProblems(from *time.Time, to *time.Time) (*ProblemsDto, error) {
	return c.GetProblemList(nil, from, to)
}

// GetProblemList is the "GET list" API call from dynatrace
// Documentation: https://www.dynatrace.com/support/help/dynatrace-api/environment-api/problems-v2/problems/get-problems-list
func (c *Config) GetProblemList(entityid *string, from *time.Time, to *time.Time) (*ProblemsDto, error) {
	requrl := c.EnvironmentURL + "/api/v2/problems"

	params := map[string]interface{}{
		"pageSize": fmt.Sprintf("%d", int(c.PageSize)),
	}

	if entityid != nil {
		params["entitySelector"] = "entityId(\"" + *entityid + "\")"
	}

	if from != nil {
		params["from"] = from.Format("2006-01-02T15:04:05")
	}

	if to != nil {
		params["to"] = to.Format("2006-01-02T15:04:05")
	}

	result := ProblemsDto{}
	nextpage, err := c.getRequest(requrl, &result, params)

	if err != nil {
		return nil, err
	}

	for nextpage != nil {
		tmp := ProblemsDto{}
		nextpage, err = c.getRequest(requrl, &tmp, map[string]interface{}{"nextPageKey": *nextpage})

		if err != nil {
			return nil, err
		}

		*result.Problems = append(*result.Problems, *tmp.Problems...)
	}

	return &result, nil
}

// PostClose is the "POST close a problem" API call from dynatrace
// Documentation: https://www.dynatrace.com/support/help/dynatrace-api/environment-api/problems-v2/problems/post-close
func (c *Config) CloseProblemV2(problemid string, comment string) error {
	requrl := c.EnvironmentURL + "/api/v2/problems/" + problemid + "/close"

	var postvar struct {
		Message string `json:"message"`
	}

	postvar.Message = comment

	result := ProblemCloseResult{}
	err := c.postRequest(requrl, &result, postvar)

	return err
}

func (c *Config) PostComment(problemid string, comment string) error {
	requrl := c.EnvironmentURL + "/api/v2/problems/" + problemid + "/comments"

	postvar := CommentRequestDtoImpl{
		Message: &comment,
	}

	err := c.postRequest(requrl, nil, postvar)

	return err
}
