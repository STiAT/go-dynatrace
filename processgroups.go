package dynatrace

import (
	"encoding/json"
)

func (c *Config) GetAllProcessGroups() (*[]ProcessGroup, error) {
	requrl := c.EnvironmentURL + "/api/v1/entity/infrastructure/process-groups"

	result := []ProcessGroup{}
	nextpage, err := c.getRequest(requrl, &result, nil)

	if err != nil {
		return nil, err
	}

	for nextpage != nil {
		tmp := []ProcessGroup{}
		nextpage, err = c.getRequest(requrl, &tmp, map[string]interface{}{"nextPageKey": *nextpage})

		if err != nil {
			return nil, err
		}

		result = append(result, tmp...)
	}

	return &result, nil

}

func (c *Config) SetProcessGroupAvailibilityMonitoring(pgid string, detection AnomalyDetectionPG) error {
	requrl := c.EnvironmentURL + "/api/config/v1/anomalyDetection/processGroups/" + pgid

	jsonStr, _ := json.Marshal(detection)
	err := c.putRequest(requrl, nil, jsonStr)

	if err != nil {
		return err
	}

	return nil
}

func (c *Config) GetProcessGroupAvailibilityMonitoring(pgid string) (*AnomalyDetectionPG, error) {
	requrl := c.EnvironmentURL + "/api/config/v1/anomalyDetection/processGroups/" + pgid

	result := AnomalyDetectionPG{}
	_, err := c.getRequest(requrl, &result, nil)

	if err != nil {
		return nil, err
	}

	return &result, nil
}
