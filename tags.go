package dynatrace

import (
	"errors"
	"net/url"
)

func (c *Config) SetTags(entityid string, tags AddEntityTags) error {
	requrl := c.EnvironmentURL + "/api/v2/tags?entitySelector=entityId(\"" + url.QueryEscape(entityid) + "\")"

	result := AddedEntityTags{}
	err := c.postRequest(requrl, &result, tags)
	return err
}

func (c *Config) DeleteTag(entityid string, key string, value *string) error {
	requrl := c.EnvironmentURL + "/api/v2/tags"
	var params map[string]interface{}
	if value != nil {
		params = map[string]interface{}{"entitySelector": "entityId(\"" + entityid + "\")", "key": key, "value:": *value}
	} else {
		params = map[string]interface{}{"entitySelector": "entityId(\"" + entityid + "\")", "key": key}
	}
	target := DeleteEntityTags{}
	err := c.deleteRequest(requrl, &target, params)

	if err != nil {
		return err
	}

	if *target.MatchedEntitiesCount == 0 {
		return errors.New("no entities matched the given search string for tag deletion")
	}

	return nil
}
