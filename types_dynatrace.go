package dynatrace

// EntityShortRepresentation from dynatrace.
type EntityShortRepresentation struct {
	ID          *string `json:"id,omitempty"`
	Name        *string `json:"name,omitempty"`
	Description *string `json:"description"`
}

// StubList from dynatrace
type StubList struct {
	Values *[]EntityShortRepresentation `json:"values,omitempty"`
}

// ConfigurationMetadata from dynatrace
type ConfigurationMetadata struct {
	ClusterVersion        *string    `json:"clusterVersion,omitempty"`
	ConfigurationVersions *[]float64 `json:"configurationVersions,omitempty"`
}

// TagInfo from dynatrace
type TagInfo struct {
	Context *string `json:"context,omitempty"`
	Key     *string `json:"key,omitempty"`
	Value   *string `json:"value,omitempty"`
}

// MonitoredEntityFilter from dynatrace
type MonitoredEntityFilter struct {
	Type           *string    `json:"type,omitempty"`
	MZID           *string    `json:"mzId,omitempty"`
	Tags           *[]TagInfo `json:"tags,omitempty"`
	TagCombination *string    `json:"tagCombination,omitempty"`
}

// Scope from dynatrace
type Scope struct {
	Entities *[]string                `json:"entities,omitempty"`
	Matches  *[]MonitoredEntityFilter `json:"matches,omitempty"`
}

// Recurrence The recurrence of the maintenance window.
type Recurrence struct {
	DayOfWeek       *string  `json:"dayOfWeek,omitempty"`
	DayOfMonth      *float64 `json:"dayOfMonth,omitempty"`
	StartTime       *string  `json:"startTime,omitempty"`
	DurationMinutes *float64 `json:"durationMinutes,omitempty"`
}

// Schedule from dynatrace
type Schedule struct {
	RecurrenceType *string     `json:"recurrenceType,omitempty"`
	Recurrence     *Recurrence `json:"recurrence,omitempty"`
	Start          *string     `json:"start,omitempty"`
	End            *string     `json:"end,omitempty"`
	ZoneID         *string     `json:"zoneId,omitempty"`
}

// AgentVersion from dynatrace
type AgentVersion struct {
	Major          *float64 `json:"major,omitempty"`
	Minor          *float64 `json:"minor,omitempty"`
	Revision       *float64 `json:"revision,omitempty"`
	Timestamp      *string  `json:"timestamp,omitempty"`
	SourceRevision *string  `json:"sourceRevision,omitempty"`
}

// HostGroup from dynatrace
type HostGroup struct {
	Meld *string `json:"meld,omitempty"`
	Name *string `json:"name,omitempty"`
}

// TechnologyInfo from dynatrace
type TechnologyInfo struct {
	Type    *string `json:"string,omitempty"`
	Edition *string `json:"edition,omitempty"`
	Version *string `json:"version,omitempty"`
}

// EntityStub from dynatrace
type EntityStub struct {
	EntityID *EntityID `json:"entityId,omitempty"`
	Name     *string   `json:"name,omitempty"`
}

// EntityID from dynatrace
type EntityID struct {
	ID   *string `json:"id,omitempty"`
	Type *string `json:"type,omitempty"`
}

// ProblemsDto from dynatrace
type ProblemsDto struct {
	TotalCount  *float64     `json:"totalCount,omitempty"`
	PageSize    *float64     `json:"pageSize,omitempty"`
	NextPageKey *string      `json:"nextPageKey,omitempty"`
	Problems    *[]ProblemV2 `json:"problems,omitempty"`
}

// MaintenanceWindow Configuration of a maintenance window.
type MaintenanceWindow struct {
	Metadata    *ConfigurationMetadata `json:"metadata,omitempty"`
	ID          *string                `json:"id,omitempty"`
	Name        *string                `json:"name,omitempty"`
	Description *string                `json:"description,omitempty"`
	Type        *string                `json:"type,omitempty"`
	Suppression *string                `json:"suppression,omitempty"`
	Scope       *Scope                 `json:"scope,omitempty"`
	Schedule    *Schedule              `json:"schedule,omitempty"`
}

// Event type from dynatrace
type Event struct {
	StartTime                                 *float64                     `json:"startTime,omitempty"`
	EndTime                                   *float64                     `json:"endTime,omitempty"`
	EntityID                                  *string                      `json:"entityID,omitempty"`
	EntityName                                *string                      `json:"entityName,omitempty"`
	SeverityLevel                             *string                      `json:"severityLevel,omitempty"`
	ImpactLevel                               *string                      `json:"impactLevel,omitempty"`
	EventType                                 *string                      `json:"eventType,omitempty"`
	ResourceID                                *string                      `json:"resourceId,omitempty"`
	ResourceName                              *string                      `json:"resourceName,omitempty"`
	Status                                    *string                      `json:"status,omitempty"`
	Severities                                *[]EventSeverity             `json:"severities,omitempty"`
	IsRootCause                               *bool                        `json:"isRootCause,omitempty"`
	DeploymentProject                         *string                      `json:"deploymentProject,omitempty"`
	CPULimitInMHz                             *float64                     `json:"cpuLimitInMHz,omitempty"`
	DeploymentParamAdded                      *string                      `json:"deploymentParamAdded,omitempty"`
	AffectedPrivateSyntheticLocations         *[]string                    `json:"affectedPrivateSyntheticLocations,omitempty"`
	IsClusterWide                             *bool                        `json:"isClusterWide,omitempty"`
	Source                                    *string                      `json:"source,omitempty"`
	EffectiveEntity                           *string                      `json:"effectiveEntity,omitempty"`
	OperatingSystem                           *string                      `json:"operatingSystem,omitempty"`
	Artifact                                  *string                      `json:"artifact,omitempty"`
	CPULoad                                   *float64                     `json:"cpuLoad,omitempty"`
	AffectedRequestsPerMinute                 *float64                     `json:"affectedRequestsPerMinute,omitempty"`
	AnnotationDescription                     *string                      `json:"annotationDescription,omitempty"`
	Browser                                   *string                      `json:"browser,omitempty"`
	AffectedSyntheticLocations                *[]string                    `json:"affectedSyntheticLocations,omitempty"`
	DeploymentName                            *string                      `json:"deploymentName,omitempty"`
	DeploymentParamRemoved                    *string                      `json:"deploymentParamRemoved,omitempty"`
	CorrelationID                             *string                      `json:"correlationId,omitempty"`
	UserDefined50thPercentileThreshold        *float64                     `json:"userDefined50thPercentileThreshold,omitempty"`
	SyntheticErrorType                        *[]string                    `json:"syntheticErrorType,omitempty"`
	ServiceMethodGroup                        *string                      `json:"serviceMethodGroup,omitempty"`
	ReferenceResponseTime90thPercentile       *float64                     `json:"referenceResponseTime90thPercentile,omitempty"`
	UserAction                                *string                      `json:"userAction,omitempty"`
	MinimumProcessGroupInstanceCountThreshold *float64                     `json:"minimumProcessGroupInstanceCountThreshold,omitempty"`
	ReferenceResponseTime50thPercentile       *float64                     `json:"referenceResponseTime50thPercentile,omitempty"`
	Original                                  *string                      `json:"original,omitempty"`
	UserDefined90thPercentileThreshold        *float64                     `json:"userDefined90thPercentileThreshold,omitempty"`
	DeploymentVersion                         *string                      `json:"deploymentVersion,omitempty"`
	AnnotationType                            *string                      `json:"string,omitempty"`
	AffectedSyntheticActions                  *[]string                    `json:"affectedSyntheticActions,omitempty"`
	AffectedUserActionsPerMinute              *float64                     `json:"affectedUserActionsPerMinute,omitempty"`
	ActiveMaintenanceWindows                  *[]EntityShortRepresentation `json:"activeMaintenanceWindows,omitempty"`
	MobileAppVersion                          *string                      `json:"mobileAppVersion,omitempty"`
	UserDefinedFailureRateThreshold           *float64                     `json:"userDefinedFailureRateThreshold,omitempty"`
	Percentile                                *string                      `json:"percentile,omitempty"`
	CustomProperties                          map[string]string            `json:"customProperties,omitempty"`
	RemediationAction                         *string                      `json:"remediationAction,omitempty"`
	Service                                   *string                      `json:"service,omitempty"`
	CIBackLink                                *string                      `json:"ciBackLink,omitempty"`
	Geolocation                               *string                      `json:"geolocation,omitempty"`
	ServiceMethod                             *string                      `json:"serviceMethod,omitempty"`
	Changed                                   *string                      `json:"changed,omitempty"`
}

// EventSeverity from dynatrace
type EventSeverity struct {
	Context *string  `json:"context,omitempty"`
	Value   *float64 `json:"value,omitempty"`
	Unit    *string  `json:"unit,omitempty"`
}

// EventRestImpact from dynatrace
type EventRestImpact struct {
	EntityID      *string `json:"entityId,omitempty"`
	EntityName    *string `json:"entityName,omitempty"`
	SeverityLevel *string `json:"severityLevel,omitempty"`
	ImpactLevel   *string `json:"impactLevel,omitempty"`
	EventType     *string `json:"eventType,omitempty"`
	ResourceID    *string `json:"resourceId,omitempty"`
	ResourceName  *string `json:"resourceName,omitempty"`
}

// EventRestEntry from dynatrace
type EventRestEntry struct {
	StartTime     *float64   `json:"startTime,omitempty"`
	EndTime       *float64   `json:"endTime,omitempty"`
	EntityID      *string    `json:"entityId,omitempty"`
	EntityName    *string    `json:"entityName,omitempty"`
	SeverityLevel *string    `json:"severityLevel,omitempty"`
	ImpactLevel   *string    `json:"impactLevel,omitempty"`
	EventType     *string    `json:"impactType,omitempty"`
	ResourceID    *string    `json:"resourceId,omitempty"`
	ResourceName  *string    `json:"resourceName,omitempty"`
	EventStatus   *string    `json:"eventStatus,omitempty"`
	Tags          *[]TagInfo `json:"tags,omitempty"`
	ID            *string    `json:"id,omitempty"`
}

// Host Information about the host.
type Host struct {
	EntityID                   *string                      `json:"entityid,omitempty"`
	DisplayName                *string                      `json:"displayName,omitempty"`
	CustomizedName             *string                      `json:"customizedName,omitempty"`
	DiscoveredName             *string                      `json:"discoveredName,omitempty"`
	FirstSeenTimestamp         *float64                     `json:"firstSeenTimestamp,omitempty"`
	LastSeenTimestamp          *float64                     `json:"lastSeenTimestamp,omitempty"`
	Tags                       *[]TagInfo                   `json:"tags,omitempty"`
	ToRelationships            interface{}                  `json:"object,omitempty"`
	GCEProjectID               *string                      `json:"gceProjectId,omitempty"`
	OpenStackSecurityGroups    *[]string                    `json:"openstackSecurityGroups,omitempty"`
	CloudType                  *string                      `json:"cloudType,omitempty"`
	AWSInstanceType            *string                      `json:"awsInstanceType,omitempty"`
	AzureComputeModeName       *string                      `json:"azureComputMOdeName,omitempty"`
	BoshDeploymentID           *string                      `json:"boshDeploymentId,omitempty"`
	Bitness                    *string                      `json:"bitness,omitempty"`
	BoshAvailibilityZone       *string                      `json:"boshAvailibilityZone,omitempty"`
	AWSSecurityGroup           *[]string                    `json:"awsSecurityGroup,omitempty"`
	BeanstalkEnvironmentName   *string                      `json:"beanstalkEnvironmentName,omitempty"`
	AzureSku                   *string                      `json:"azureSku,omitempty"`
	GCEInstanceID              *string                      `json:"gceInstanceId,omitempty"`
	OpenStackComputeNodeName   *string                      `json:"openstackComputeNodeName,omitempty"`
	OpenStackInstaceType       *string                      `json:"openStackInstaceType,omitempty"`
	GCEProject                 *string                      `json:"gceProject,omitempty"`
	CPUCores                   *float64                     `json:"cpuCores,omitempty"`
	BoshInstanceName           *string                      `json:"boshInstanceName,omitempty"`
	OSType                     *string                      `json:"osType,omitempty"`
	AzureHostNames             *[]string                    `json:"azureHostNames,omitempty"`
	ConsumedHostUnits          *float64                     `json:"consumedHostUnits,omitempty"`
	AWSInstanceID              *string                      `json:"awsInstanceId,omitempty"`
	OpenStackProjectName       *string                      `json:"openstackProjectName,omitempty"`
	VMwareName                 *string                      `json:"vmwareName,omitempty"`
	IsMonitoringCandidate      *bool                        `json:"isMonitoringCandidate,omitempty"`
	CloudPlatformVendorVersion *string                      `json:"cloudPlatformVendorVersion,omitempty"`
	UserLevel                  *string                      `json:"userLevel,omitempty"`
	AgentVersion               *AgentVersion                `json:"agentVersion,omitempty"`
	PaaSType                   *string                      `json:"paasType,omitempty"`
	AutoScalingGroup           *string                      `json:"autoScalingGroup,omitempty"`
	ZOSTotalPhysicalMemory     *float64                     `json:"zosTotalPhysicalMemory,omitempty"`
	GCPZone                    *string                      `json:"gcpZone,omitempty"`
	KubernetesNode             *string                      `json:"kubernetesNode,omitempty"`
	ZOSCPUModelNumber          *string                      `json:"zosCPUModelNumber,omitempty"`
	AzureVMName                *string                      `json:"azureVmName,omitempty"`
	OSVersion                  *string                      `json:"osVersion,omitempty"`
	AmiID                      *string                      `json:"amiId,omitempty"`
	LogicalCPUCores            *float64                     `json:"logicalCpuCores,omitempty"`
	IPAddresses                *[]string                    `json:"ipAddresses,omitempty"`
	ZOSTotalZiipProcessors     *float64                     `json:"zosTotalZiipProcessors,omitempty"`
	NetworkZoneID              *string                      `json:"networkZoneId,omitempty"`
	MonitoringMode             *string                      `json:"monitoringMode,omitempty"`
	ScaleSetName               *string                      `json:"scaleSetName,omitempty"`
	KubernetesCluster          *string                      `json:"kubernetesCluster,omitempty"`
	KubernetesLabels           interface{}                  `json:"kubernetesLabels,omitempty"`
	SimultaneousMultithreading *float64                     `json:"simultaneousMultithreading,omitempty"`
	HostGroup                  *HostGroup                   `json:"hostGroup,omitempty"`
	ZOSCPUSerialNumber         *string                      `json:"zosCPUSerialNumber,omitempty"`
	ManagementZones            *[]EntityShortRepresentation `json:"managementZones,omitempty"`
	GCEInstanceName            *string                      `json:"gceInstanceName,omitempty"`
	OpenStackAvZone            *string                      `json:"openstackAvZone,omitempty"`
	LogicalCPUs                *float64                     `json:"logicalCpus,omitempty"`
	ESXIHostName               *string                      `json:"esxiHostName,omitempty"`
	VirtualCPUs                *float64                     `json:"virtualCpus,omitempty"`
	OneAgentCustomHostName     *string                      `json:"oneAgentCustomHostName,omitempty"`
	ZOSVirtualization          *string                      `json:"zosVirtualization,omitempty"`
	LocalHostName              *string                      `json:"localHostName,omitempty"`
	AWSNameTag                 *string                      `json:"awsNameTag,omitempty"`
	GCEMachineType             *string                      `json:"gceMachineType,omitempty"`
	HypervisorType             *string                      `json:"hypervisorType,omitempty"`
	PaaSAgentVersions          *[]AgentVersion              `json:"paasAgentVersions,omitempty"`
	AzureSiteNames             *[]string                    `json:"azureSiteNames,omitempty"`
	BoshInstanceID             *string                      `json:"boshInstanceId,omitempty"`
	PublicHostName             *string                      `json:"publicHostName,omitempty"`
	PaaSMemoryLimit            *float64                     `json:"paasSiteNames,omitempty"`
	OSArchitecture             *string                      `json:"osArchitecture,omitempty"`
	LocalIP                    *string                      `json:"localIp,omitempty"`
	BoshStemcellVersion        *string                      `json:"boshStemcellVersion,omitempty"`
	OpenStackVMName            *string                      `json:"openstackVmName,omitempty"`
	GCEPublicIPAddresses       *[]string                    `json:"gcePublicIpAddresses,omitempty"`
	BoshName                   *string                      `json:"boshName,omitempty"`
	PublicIP                   *string                      `json:"publicIp,omitempty"`
	SoftwareTechnologies       *[]TechnologyInfo            `json:"softwareTechnologies,omitempty"`
	FromRelationships          interface{}                  `json:"fromRelationships,omitempty"`
}

// ProblemV2 from dynatrace
type ProblemV2 struct {
	AffectedEntities  *[]EntityStub          `json:"affectedEntities,omitempty"`
	DisplayID         *string                `json:"displayId,omitempty"`
	ImpactedEntities  *[]EntityStub          `json:"impactedEntities,omitempty"`
	ImpactLevel       *string                `json:"impactLevel,omitempty"`
	EntityTags        *[]METag               `json:"entityTags,omitempty"`
	LinkedProblemInfo *LinkedProblem         `json:"linkedProblemInfo,omitempty"`
	RecentComments    *CommentsList          `json:"recentComments,omitempty"`
	ImpactAnalysis    *ImpactAnalysis        `json:"impactAnalysis,omitempty"`
	RootCauseEntity   *EntityStub            `json:"EntityStub,omitempty"`
	ProblemFilters    *[]AlertingProfileStub `json:"problemFilters,omitempty"`
	EvidenceDetails   *EvidenceDetails       `json:"evidenceDetails,omitempty"`
	ManagementZones   *[]ManagementZone      `json:"managementZones,omitempty"`
	ProblemID         *string                `json:"problemId,omitempty"`
	SeverityLevel     *string                `json:"severityLevel,omitempty"`
	Status            *string                `json:"status,omitempty"`
	StartTime         *float64               `json:"startTime,omitempty"`
	EndTime           *float64               `json:"endTime,omitempty"`
	Title             *string                `json:"title,omitempty"`
}

// ProblemV1 from dynatrace
type ProblemV1 struct {
	ID                     *string            `json:"id,omitempty"`
	StartTime              *float64           `json:"startTime,omitempty"`
	EndTime                *float64           `json:"endTime,omitempty"`
	DisplayName            *string            `json:"displayName,omitempty"`
	ImpactLevel            *string            `json:"impactLevel,omitempty"`
	Status                 *string            `json:"status,omitempty"`
	SeverityLevel          *string            `json:"severityLevel,omitempty"`
	CommentCount           *float64           `json:"commentCount,omitempty"`
	TagsOfAffectedEntities *[]TagInfo         `json:"tagsOfAffectedEntities,omitempty"`
	RankedEvents           *[]Event           `json:"rankedEvents,omitempty"`
	RankedImpacts          *[]EventRestImpact `json:"rankedImpacts,omitempty"`
	AffectedCounts         map[string]float64 `json:"affectedCounts,omitempty"`
	RecoveredCounts        map[string]float64 `json:"recoveredCounts,omitempty"`
	HasRootCause           *bool              `json:"hasRootCause,omitempty"`
}

// METag from dynatrace
type METag struct {
	StringRepresentation *string `json:"stringRepresentation,omitempty"`
	Value                *string `json:"value,omitempty"`
	Key                  *string `json:"key,omitempty"`
	Context              *string `json:"context,omitempty"`
}

// ManagementZone from dynatrace
type ManagementZone struct {
	Name *string `json:"name,omitempty"`
	ID   *string `json:"id,omitempty"`
}

// EvidenceDetails from dynatrace
type EvidenceDetails struct {
	TotalCount *float64    `json:"totalCount,omitempty"`
	Details    *[]Evidence `json:"details,omitempty"`
}

// Evidence from dynatrace
type Evidence struct {
	EvidenceType      *string     `json:"evidenceType,omitempty"`
	DisplayName       *string     `json:"displayName,omitempty"`
	Entity            *EntityStub `json:"entity,omitempty"`
	GroupingEntity    *EntityStub `json:"groupingEntity,omitempty"`
	RootCauseRelevant *bool       `json:"rootCauseRelevant,omitempty"`
	StartTime         *float64    `json:"startTime,omitempty"`
}

// AlertingProfileStub from dynatrace
type AlertingProfileStub struct {
	Name *string `json:"name,omitempty"`
	ID   *string `json:"id,omitempty"`
}

// ImpactAnalysis from dynatrace
type ImpactAnalysis struct {
	Impacts *[]Impact `json:"impacts,omitempty"`
}

// Impact from dynatrace
type Impact struct {
	ImpactType             *string     `json:"impactType,omitempty"`
	ImpactedEntity         *EntityStub `json:"impactedEntity,omitempty"`
	EstimatedAffectedUsers *float64    `json:"estimatedAffectedUsers,omitempty"`
}

// CommentsList from dynatrace
type CommentsList struct {
	Comments    *[]Comment `json:"comments,omitempty"`
	NextPageKey *string    `json:"nextPageKey,omitempty"`
	PageSize    *float64   `json:"pageSize,omitempty"`
	TotalCount  *float64   `json:"totalCount,omitempty"`
}

// Comment from dynatrace
type Comment struct {
	CreatedAtTimestamp *float64 `json:"createdAtTimestamp,omitempty"`
	AuthorName         *string  `json:"authorName,omitempty"`
	Context            *string  `json:"context,omitempty"`
	ID                 *string  `json:"id,omitempty"`
	Content            *string  `json:"content,omitempty"`
}

// LinkedProblem from dynatrace
type LinkedProblem struct {
	DisplayID *string `json:"displayId,omitempty"`
	ProblemID *string `json:"problemId,omitempty"`
}

// EventCreation from dynatrace
type EventCreation struct {
	EventType             *string               `json:"eventType,omitempty"`
	Start                 *float64              `json:"start,omitempty"`
	End                   *float64              `json:"end,omitempty"`
	TimeoutMinutes        *float64              `json:"timeoutMinutes,omitempty"`
	AttachRules           *PushEventAttachRules `json:"attachRules,omitempty"`
	CustomProperties      map[string]string     `json:"customProperties,omitempty"`
	Source                *string               `json:"source,omitempty"`
	AnnotationType        *string               `json:"annotationType,omitempty"`
	AnnotationDescription *string               `json:"annotationDescription,omitempty"`
	Description           *string               `json:"description,omitempty"`
	DeploymentName        *string               `json:"deploymentName,omitempty"`
	DeploymentVersion     *string               `json:"deploymentVersion,omitempty"`
	TimeseriesIDs         *[]string             `json:"timeseriesIds,omitempty"`
	DeploymentProject     *string               `json:"deploymentProject,omitempty"`
	CIBackLink            *string               `json:"ciBackLink,omitempty"`
	RemediationAction     *string               `json:"remediationAction,omitempty"`
	Original              *string               `json:"original,omitempty"`
	Changed               *string               `json:"changed,omitempty"`
	Configuration         *string               `json:"configuration,omitempty"`
	Title                 *string               `json:"title,omitempty"`
	AllowDavisMerge       *bool                 `json:"allowDavisMerge,omitempty"`
}

// PushEventAttachRules from dynatrace
type PushEventAttachRules struct {
	EntityIDs *[]string       `json:"entityIds,omitempty"`
	TagRule   *[]TagMatchRule `json:"tagRule,omitempty"`
}

// TagMatchRule from dynatrace
type TagMatchRule struct {
	METypes *[]string  `json:"meTypes,omitempty"`
	Tags    *[]TagInfo `json:"tags,omitempty"`
}

// ProblemFeedResultWrapper from dynatrace
type ProblemFeedResultWrapper struct {
	Result *ProblemFeedQueryResult `json:"result,omitempty"`
}

// ProblemFeedQueryResult from dynatrace
type ProblemFeedQueryResult struct {
	Problems  *[]ProblemV1       `json:"problems,omitempty"`
	Monitored map[string]float64 `json:"monitored,omitempty"`
}

// ProblemDetailsResultWrapper from dynatrace
type ProblemDetailsResultWrapper struct {
	Result *ProblemV1 `json:"result,omitempty"`
}

// AddEntityTag from dynatrace
type AddEntityTag struct {
	Key   *string `json:"key,omitempty"`
	Value *string `json:"value,omitempty"`
}

// AddEntityTags from dynatrace
type AddEntityTags struct {
	Tags *[]AddEntityTag `json:"tags,omitempty"`
}

// DeleteEntityTags from dynatrace
type DeleteEntityTags struct {
	MatchedEntitiesCount *float64 `json:"matchedEntitiesCount,omitempty"`
}

// CustomDevicePushResult from dynatrace
type CustomDevicePushResult struct {
	EntityID *string `json:"entityId,omitempty"`
	GroupID  *string `json:"groupId,omitempty"`
}

// EventStoreResult from dynatrace
type EventStoreResult struct {
	StoredEventIDs       *[]float64 `json:"storedEventIds,omitempty"`
	StoredIDs            *[]string  `json:"storedIds,omitempty"`
	StoredCorrelationIDs *[]string  `json:"storedCorrelationIds,omitempty"`
}

// ProblemCloseResult from dynatrace
type ProblemCloseResult struct {
	ProblemID      *string  `json:"problemId,omitempty"`
	Closing        *bool    `json:"closing,omitempty"`
	CloseTimestamp *float64 `json:"closeTimestamp,omitempty"`
	Comment        *Comment `json:"comment,omitempty"`
}

// AddedEntityTags from dynatrace
type AddedEntityTags struct {
	MatchedEntitiesCount *float64 `json:"matchedEntitiesCount,omitempty"`
	AppliedTags          *[]METag `json:"appliedTags,omitempty"`
}

// EntitiesList from dynatrace
type EntitiesList struct {
	TotalCount  *float64  `json:"totalCount,omitempty"`
	PageSize    *float64  `json:"pageSize,omitempty"`
	NextPageKey *string   `json:"nextPageKey,omitempty"`
	Entities    *[]Entity `json:"entities,omitempty"`
}

// Entity from dynatrace
type Entity struct {
	Tags              *[]METag                       `json:"tags,omitempty"`
	EntityID          *string                        `json:"entityId,omitempty"`
	LastSeenTms       *float64                       `json:"lastSeenTms,omitempty"`
	FirstSeenTms      *float64                       `json:"firstSeenTms,omitempty"`
	ManagementZones   *[]ManagementZone              `json:"managementZones,omitempty"`
	FromRelationships map[string]interface{}         `json:"fromRelationships,omitempty"`
	ToRelationships   map[string][]map[string]string `json:"toRelationships,omitempty"`
	Properties        map[string]interface{}         `json:"properties,omitempty"`
	DisplayName       *string                        `json:"displayName,omitempty"`
}

// ProcessGroup from Dynatrace
type ProcessGroup struct {
	EntityID             *string                      `json:"entityId,omitempty"`
	DisplayName          *string                      `json:"displayName,omitempty"`
	CustomizedName       *string                      `json:"customizedName,omitempty"`
	DiscoveredName       *string                      `json:"discoveredName,omitempty"`
	FirstSeenTimestamp   *float64                     `json:"firstSeenTimestamp,omitempty"`
	LastSeenTimestamp    *float64                     `json:"lastSeenTimestamp,omitempty"`
	Tags                 *[]TagInfo                   `json:"tags,omitempty"`
	ToRelationships      interface{}                  `json:"toRelationships,omitempty"`
	Metadata             interface{}                  `json:"metadata,omitempty"`
	SoftwareTechnologies *[]TechnologyInfo            `json:"softwareTechnologies,omitempty"`
	AzureSiteName        *string                      `json:"azureSiteName,omitempty"`
	ManagementZones      *[]EntityShortRepresentation `json:"managementZones,omitempty"`
	ListenPorts          *[]float64                   `json:"listenPorts,omitempty"`
	AzureHostName        *string                      `json:"azureHostName,omitempty"`
	FromRelationships    interface{}                  `json:"fromRelationshios,omitempty"`
}

// ActiveGateList from dynatrace
type ActiveGateList struct {
	ActiveGates *[]ActiveGate `json:"activeGates,omitempty"`
}

// ActiveGate from dynatrace
type ActiveGate struct {
	ID                 *string                     `json:"id,omitempty"`
	NetworkAddresses   *[]string                   `json:"networkAddresses,omitempty"`
	OSType             *string                     `json:"osType,omitempty"`
	AutoUpdateStatus   *string                     `json:"autoUpdateStatus,omitempty"`
	OfflineSince       *float64                    `json:"offlineSince,omitempty"`
	Version            *string                     `json:"version,omitempty"`
	Type               *string                     `json:"type,omitempty"`
	Hostname           *string                     `json:"hostname,omitempty"`
	MainEnvironment    *string                     `json:"mainEnvironment,omitempty"`
	Environments       *[]string                   `json:"environments,omitempty"`
	AutoUpdateSettings *ActiveGateAutoUpdateConfig `json:"autoUpdateSetttings,omitempty"`
	NetworkZone        *string                     `json:"networkZone,omitempty"`
	Group              *string                     `json:"group,omitempty"`
	Modules            *[]ActiveGateModule         `json:"modules,omitempty"`
	Containerized      *bool                       `json:"containerized,omitempty"`
}

// ActiveGateModule from dynatrace
type ActiveGateModule struct {
	Misconfigured *bool             `json:"misconfigured,omitempty"`
	Version       *string           `json:"version,omitempty"`
	Type          *string           `json:"type,omitempty"`
	Attributes    map[string]string `json:"attributes,omitempty"`
	Enabled       *bool             `json:"enabled,omitempty"`
}

// ActiveGateAutoUpdateConfig from dynatrace
type ActiveGateAutoUpdateConfig struct {
	Setting          *string `json:"setting,omitempty"`
	EffectiveSetting *string `json:"effectiveSetting,omitempty"`
}

// NotificationConfig from dynatrace
type NotificationConfig struct {
	ID              *string `json:"id,omitempty"`
	Name            *string `json:"name,omitempty"`
	AlertingProfile *string `json:"alertingProfile,omitempty"`
	Active          *bool   `json:"active,omitempty"`
	Type            *string `json:"type,omitempty"`
	Url             *string `json:"url,omitempty"`
}

// NotificationConfigStubListDto from dynatrace
type NotificationConfigStubListDto struct {
	Values *[]NotificationConfigStub `json:"values,omitempty"`
}

// NotificationConfigStub from dynatrace
type NotificationConfigStub struct {
	ID          *string `json:"id,omitempty"`
	Name        *string `json:"name,omitempty"`
	Description *string `json:"description,omitempty"`
	Type        *string `json:"type,omitempty"`
}

// CommentRequestDtoImpl from dynatrace
type CommentRequestDtoImpl struct {
	Message *string `json:"message,omitempty"`
	Context *string `json:"context,omitempty"`
}

// AnomalyDetectionPG from dynatrace
type AnomalyDetectionPG struct {
	AvailabilityMonitoring *struct {
		Method           *string  `json:"method,omitempty"`
		MinimumThreshold *float64 `json:"minimumThreshold,omitempty"`
	} `json:"availabilityMonitoring,omitempty"`
}

// EntityTypeList from dynatrace
type EntityTypeList struct {
	TotalCount  *float64      `json:"totalCount,omitempty"`
	PageSize    *float64      `json:"pageSize,omitempty"`
	NextPageKey *string       `json:"nextPageKey,omitempty"`
	Types       *[]EntityType `json:"types,omitempty"`
}

// EntityType from dynatrace
type EntityType struct {
	EntityLimitExceeded *bool                    `json:"entityLimitExceeded,omitempty"`
	DimensionKey        *string                  `json:"dimensionKey,omitempty"`
	ManagementZones     *string                  `json:"managementZones,omitempty"`
	FromRelationships   *[]ToPosition            `json:"fromRelationships,omitempty"`
	ToRelationships     *[]FromPosition          `json:"toRelationships,omitempty"`
	Tags                *string                  `json:"tags,omitempty"`
	Properties          *[]EntityTypePropertyDto `json:"properties,omitempty"`
	Type                *string                  `json:"type,omitempty"`
	DisplayName         *string                  `json:"displayName,omitempty"`
}

// ToPosition from dynatrace
type ToPosition struct {
	ToTypes *[]string `json:"toTypes,omitempty"`
	ID      *string   `json:"id,omitempty"`
}

// FromPosition from dynatrace
type FromPosition struct {
	FromTypes *[]string `json:"fromTypes,omitempty"`
	ID        *string   `json:"id,omitempty"`
}

// EntityTypePropertyDto from dynatrace
type EntityTypePropertyDto struct {
	ID          *string `json:"id,omitempty"`
	Type        *string `json:"type,omitempty"`
	DisplayName *string `json:"displayName,omitempty"`
}

// ActiveGateGlobalAutoUpdateConfig from dynatrace
type ActiveGateGlobalAutoUpdateConfig struct {
	GlobalSetting *string                `json:"globalSetting,omitempty"`
	Metadata      *ConfigurationMetadata `json:"metadata,omitempty"`
}

// UpdateJobList from dynatrace
type UpdateJobList struct {
	UpdateJobs *[]UpdateJob `json:"updateJobs,omitempty"`
	AGId       *string      `json:"agId,omitempty"`
}

// UpdateJobList from dynatrace
type UpdateJob struct {
	UpdateMethod  *string   `json:"updateMethod,omitempty"`
	JobState      *string   `json:"jobState,omitempty"`
	StartVersion  *string   `json:"startVersion,omitempty"`
	Environments  *[]string `json:"environments,omitempty"`
	JobID         *string   `json:"jobid,omitempty"`
	UpdateType    *string   `json:"updateType,omitempty"`
	AGType        *string   `json:"agType,omitempty"`
	Cancelable    *bool     `json:"cancelable,omitempty"`
	Duration      *float64  `json:"duration,omitempty"`
	Timestamp     *float64  `json:"timestamp,omitempty"`
	TargetVersion *string   `json:"targetVersion,omitempty"`
	Error         *string   `json:"error,omitempty"`
}

// UpdateJobsAll from dynatrace
type UpdateJobsAll struct {
	AllUpdateJobs *[]UpdateJobList `json:"allUpdateJobs,omitempty"`
}

// AnonymizationIdResult by dynatrace
type AnonymizationIdResult struct {
	ClusterRequestIDs *[]AnonymizationClusterRequestID `json:"clusterRequestIds,omitempty"`
	RequestID         *string                          `json:"requestId,omitempty"`
}

// AnonymizationClusterRequestID from dynatrace
type AnonymizationClusterRequestID struct {
	ID     *float64 `json:"id,omitempty"`
	DCName *string  `json:"dcName,omitempty"`
}

// AnonymizationProgressResult from dynatrace
type AnonymizationProgressResult struct {
	Progress *float64 `json:"progress,omitempty"`
}

// AuditLog from dynatrace
type AuditLog struct {
	TotalCount  *float64         `json:"totalCount,omitempty"`
	PageSize    *float64         `json:"pageSize,omitempty"`
	NextPageKey *string          `json:"nextPageKey,omitempty"`
	AuditLogs   *[]AuditLogEntry `json:"auditLogs,omitempty"`
}

// AuditLogEntry from dynatrace
type AuditLogEntry struct {
	LogID         *string     `json:"logId,omitempty"`
	EventType     *string     `json:"eventType,omitempty"`
	Category      *string     `json:"category,omitempty"`
	EntityId      *string     `json:"entityId,omitempty"`
	EnvironmentId *string     `json:"environmentId,omitempty"`
	User          *string     `json:"user,omitempty"`
	UserType      *string     `json:"userType,omitempty"`
	UserOrigin    *string     `json:"userOrigin,omitempty"`
	Timestamp     *float64    `json:"timestamp,omitempty"`
	Success       *bool       `json:"success,omitempty"`
	Message       *string     `json:"message,omitempty"`
	Patch         interface{} `json:"patch,omitempty"`
}

// ClusterVersion from dynatrace
type ClusterVersion struct {
	Version *string `json:"version,omitempty"`
}

// CustomEntityTags from dynatrace
type CustomEntityTags struct {
	Tags       *[]METag `json:"tags,omitempty"`
	TotalCount *float64 `json:"totalCount,omitempty"`
}

// DeletedEntityTags from dynatrace
type DeletedEntityTags struct {
	MatchedEntitiesCount *float64 `json:"matchedEntitiesCount,omitempty"`
}

// DavisSecurityAdviceList from dynatrace
type DavisSecurityAdviceList struct {
	TotalCount  *float64               `json:"totalCount,omitempty"`
	PageSize    *float64               `json:"pageSize,omitempty"`
	NextPageKey *string                `json:"nextPageKey,omitempty"`
	Advices     *[]DavisSecurityAdvice `json:"advices,omitempty"`
}

// DavisSecurityAdvice from dynatrace
type DavisSecurityAdvice struct {
	Name                *string   `json:"name,omitempty"`
	VulnerableComponent *string   `json:"vulnerableComponent,omitempty"`
	Technology          *string   `json:"technology,omitempty"`
	AdviceType          *string   `json:"adviceType,omitempty"`
	Critical            *[]string `json:"critical,omitempty"`
	High                *[]string `json:"high,omitempty"`
	Medium              *[]string `json:"medium,omitempty"`
	Low                 *[]string `json:"low,omitempty"`
	None                *[]string `json:"none,omitempty"`
}
