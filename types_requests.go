package dynatrace

// UpdateJobsRequest custom defined structure for dynatrace update job request / updates
// Used in:
// * activegate.go: GetAllAutoUpdateJobs, GetActiveGatesWithAutoUpdateJobs
type UpdateJobsRequest struct {
	AGId                     string  `json:"agId,omitempty"`
	From                     *string `json:"from,omitempty"`
	To                       *string `json:"to,omitempty"`
	StartVersionCompareType  *string `json:"startVersionCompareType,omitempty"`
	StartVersion             *string `json:"startVersion,omitempty"`
	UpdateType               *string `json:"updateType,omitempty"`
	TargetVersionCompareType *string `json:"targetVersionCompareType,omitempty"`
	TargetVersion            *string `json:"targetVersion,omitempty"`
	LastUpdates              bool    `json:"lastUpdates,omitempty"`
}

// AnonymizationJobRequest custom defined structure for PUT requests for anonymization jobs
// Used in:
// anonymization.go: PutAnonymizationJob
type AnonymizationJobRequest struct {
	StartTimestamp  float64  `json:"startTimestamp,omitempty"`
	EndTimestamp    float64  `json:"endTimestamp,omitempty"`
	UserIDs         []string `json:"userIds,omitempty"`
	IPs             []string `json:"ips,omitempty"`
	AdditionalField []string `json:"additionalField,omitempty"`
}

// AuditLogRequest custom defined structure for log requests
// Used in:
// auditlogs.go: GetAuditLogs
type AuditLogRequest struct {
	NextPageKey string  `json:"nextPageKey,omitempty"`
	PageSize    float64 `json:"pageSize,omitempty"`
	Filter      string  `json:"filter,omitempty"`
	From        string  `json:"from,omitempty"`
	To          string  `json:"to,omitempty"`
	Sort        string  `json:"sort,omitempty"`
}

// CustomTagRequest custom defined structure for tag requests
// Used in:
// customdevice.go: GetCustomTags, PostCustomTags
type CustomTagRequest struct {
	EntitySelector string        `json:"entitySelector"`
	From           string        `json:"from,omitempty"`
	To             string        `json:"to,omitempty"`
	Body           AddEntityTags `json:"body,omitempty"`
}

// CustomTagDeleteRequest custom defined structure for tag requests
// Used in:
// customdevice.go: DeleteCustomTags
type CustomTagDeleteRequest struct {
	Key              string `json:"key"`
	Value            string `json:"value,omitempty"`
	DeleteAllWithKey bool   `json:"deleteAllWithKey,omitempty"`
	EntitySelector   string `json:"entitySelector,omitempty"`
	From             string `json:"from,omitempty"`
	To               string `json:"to,omitempty"`
}

// DavisSecurityAdvisorRequest custom defined structure for davis security advisor requests
// Used in:
// davissecurityadvisor.go: DavisSecurityAdvisor
type DavisSecurityAdvisorRequest struct {
	ManagementZoneFilter string  `json:"managementZoneFilter,omitempty"`
	NextPageKey          string  `json:"nextPageKey,omitempty"`
	PageSize             float64 `json:"pageSize,omitempty"`
}
