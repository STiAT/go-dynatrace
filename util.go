package dynatrace

import (
	"encoding/json"
	"io"
	"os"
)

// Utility functions

// append to a slice only if it's not already in the slice - string based
func appendIfMissing(slice []string, s string) []string {
	for _, ele := range slice {
		if ele == s {
			return slice
		}
	}
	return append(slice, s)
}

// getTestConfig get test configuration (test.json) in the local directory for running tests
// The test.json includes the configuration required to access the dynatrace environment
// dynatrace.go: type Config struct {...}
func getTestConfig() (*Config, error) {
	jsonFile, err := os.Open("test.json")
	if err != nil {
		return nil, err
	}

	conf, _ := io.ReadAll(jsonFile)
	var config Config
	json.Unmarshal(conf, &config)

	return &config, nil
}
